let nombre = prompt("¿Cómo te llamas?");

let dia = prompt("¿Cuál es tu dia de nacimiento?");
let mes = prompt("¿Cuál es tu mes de nacimiento?");
let año = prompt("¿Cuál es tu año de nacimiento?");

let edad = 2021 - año;

if(dia > 31){
    alert("Recuerda que los meses tienen maximo 31 dias");
    location.reload();
}else if(dia < 0){
    alert("Recuerda que no existen dias negativos");
    location.reload();
}else if(mes > 12){
    alert("Recuerda que solo existen 12 meses");
    location.reload();
}else if(mes < 0){
    alert("Recuerda que no existen meses negativos");
    location.reload();
}else{
    alert("Encantado de conocerte " + nombre + "\nNaciste el " + dia + "/" + mes + "/" + año + "\nTienes " + edad + " años");

    if(edad < 0){
        alert("Al parecer no has nacido aun ¿qué haces aqui?");
        location.reload();
    }else if(edad > 150){
        alert("En realidad las personas no viven tanto tiempo revisa tu fecha de nacimiento");
        location.reload();
    }else{
        if(edad < 18){
            alert("No puedes tener acceso a esta pagina debido a tu edad \nPor ende te redireccionaremos a otra");
            window.location.href="https://www.youtube.com/watch?v=QZShA_a-5r8";
        }else{
            alert("Adelante mi rey");
        }
    }
}
